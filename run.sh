#!/bin/bash

DEFAULT_KAFKA="192.168.0.100"
ARG1=${1:-KAFKA_BROKER=$DEFAULT_KAFKA}

sudo capstan run -e '--env='$ARG1' /python.so /user-registry/user_registry.py' -n bridge
