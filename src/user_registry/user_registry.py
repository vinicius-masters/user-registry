from os import environ

from pyjung import JungRegistry


class UserRegistry(JungRegistry):

    def __init__(self, kafka_broker, in_topic, out_topic, port, group):
        super().__init__(kafka_broker, in_topic, out_topic, port, group)

    def process_message(self, message):
        task_id = message["id"]
        task = message["task"]
        username = message["content"]["username"]

        response = {"task_id": task_id, "task": task}
        result = None

        if task == "CREATE":
            self.storage[username] = message["content"]
            self.storage[username]["devices"] = []
            self.storage[username]["rules"] = []
            result = {"created": username}

        elif username in self.storage:
            if task == "GET":
                result = self.storage[username]

            elif task == "ADD_DEVICE":
                device_id = message["content"]["device_id"]
                self.storage[username]["devices"].append(device_id)
                result = {"addedDevice": device_id, "username": username}

            elif task == "ADD_RULE":
                rule_id = message["content"]["rule_id"]
                self.storage[username]["rules"].append(rule_id)
                result = {"addedRule": rule_id, "username": username}
        else:
            result = {"error": "user does not exist"}

        response["result"] = result
        return response


def main():
    kafka_broker = environ["KAFKA_BROKER"]
    user_registry = UserRegistry(kafka_broker=kafka_broker,
                                 in_topic="user_tasks",
                                 out_topic="user_results",
                                 port="9092",
                                 group="user-registry")
    user_registry.process_tasks()
    

if __name__ == "__main__":
    main()
